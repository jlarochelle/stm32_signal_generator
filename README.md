# STM32 Signal Generator

Firmware for an STM32F415RG (on Mini-M4) used as a differential signal generator. A differential signal is generated between pins PA4 and PA5 on system reset.

## Installation

### Prerequisites

Atollic TrueSTUDIO for STM32


## Usage

The signal generation begins automatically when MCU is powered. It does not automatically loop after the sequence is over. Reset the MCU to restart test sequence. The differential signal is generated between pins PA4 and PA5.
