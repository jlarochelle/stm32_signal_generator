/**
 ******************************************************************************
 * @file    main.cpp
 * @author  Jonathan Larochelle
 * @date    9 janv. 2020
 ******************************************************************************
 */

/* Includes */
#include "stm32f4xx.h"
#include <cmath>

/* Private macro */
#define PI					3.14159
#define DAC_MAX_VALUE		4095 	// 12 bits DAC
#define DAC_MID_VALUE		2047

#define DAC_OUT_FREQ		10000
#define TIM6_PERIOD			((HSI_VALUE / DAC_OUT_FREQ) - 1)
#define SINE_WAVE_AMPL		1241
#define SINE_WAVE_FREQ		10
#define SINE_WAVE_NB_SAMPLES 	((uint32_t) DAC_OUT_FREQ / SINE_WAVE_FREQ)
#define SINE_WAVE_TOTAL_SAMPLES	(4 * DAC_OUT_FREQ)
#define PULSES_AMPL		1241
#define PULSES_FREQ		1
#define PULSES_NB_SAMPLES	(DAC_OUT_FREQ / PULSES_FREQ)
#define PULSES_DURATION	(PULSES_NB_SAMPLES / 10)
#define PULSES_TOTAL_SAMPLES	(4 * DAC_OUT_FREQ)
#define FLATLINE_AMPL	DAC_MID_VALUE
#define FLATLINE_NB_SAMPLES	100
#define FLATLINE_TOTAL_SAMPLES	(2 * DAC_OUT_FREQ)

#define RED_LED			((uint16_t)0x0001)
#define ORANGE_LED		((uint16_t)0x0002)


/* Private variables */
DAC_HandleTypeDef hdac;
TIM_HandleTypeDef  htim6;
typedef uint16_t LEDcolor_typeDef;
bool	dma_started = false;
bool	transition = false;


// Application states
#define IDLE	((uint16_t)1)
#define SINE_WAVE	((uint16_t)2)
#define PULSE_TRAIN	((uint16_t)3)
#define FLATLINE	((uint16_t)4)
typedef uint16_t	state;
state curr_state;
uint16_t sine_wave_samp_count = SINE_WAVE_TOTAL_SAMPLES;
uint16_t pulses_samp_count = PULSES_TOTAL_SAMPLES;
uint16_t flatline_samp_count = FLATLINE_TOTAL_SAMPLES;


/* Private function prototypes */
void SystemClock_Config(void);
void TIM6_Init(void);
void led_config(void);
void led_on(LEDcolor_typeDef ledColor);
void led_off(LEDcolor_typeDef ledColor);
void Error_Handler(void);
void new_state(state newState);

static void DAC_Init(void);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

/* Private functions */

/**
 **===========================================================================
 **
 **  Abstract: main program
 **
 **===========================================================================
 */
int main(void)
{
	uint16_t	sinewave_ch1[SINE_WAVE_NB_SAMPLES], sinewave_ch2[SINE_WAVE_NB_SAMPLES];
	uint16_t 	pulses_ch1[PULSES_NB_SAMPLES], pulses_ch2[PULSES_NB_SAMPLES];
	uint16_t	flatline_ch1[FLATLINE_NB_SAMPLES], flatline_ch2[FLATLINE_NB_SAMPLES];
	uint16_t 	value;


	// Set-up LED outputs
	led_config();

	// Reset of all peripherals, Initializes the Flash Interface and the Systick
	HAL_Init();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	__HAL_RCC_PWR_CLK_ENABLE();

	// Configure system clock
	SystemClock_Config();

	// Configure DAC Peripheral
	DAC_Init();

	// Configure Timer 6
	TIM6_Init();

	// Generate sine wave
	for (uint16_t i = 0; i < SINE_WAVE_NB_SAMPLES; i++){
		value = (uint16_t) rint((sinf(((2*PI)/SINE_WAVE_NB_SAMPLES)*i)+1)*SINE_WAVE_AMPL);
		sinewave_ch1[i] = value < 4096 ? value : 4095;
		sinewave_ch2[i] = SINE_WAVE_AMPL;
	}

	// Generate pulse train
	for (uint16_t i = 0; i < PULSES_NB_SAMPLES; i++){
		// Pulse is generated at end of vector to achieve signal similar to picoscope generated signal.
		if (i > (PULSES_NB_SAMPLES - PULSES_DURATION)){
			value = (uint16_t) PULSES_AMPL + DAC_MID_VALUE;
		} else {
			value = DAC_MID_VALUE;
		}

		pulses_ch1[i] = rint(value);
		pulses_ch2[i] = DAC_MID_VALUE;
	}

	// Generate flatline
	for (uint16_t i = 0; i < FLATLINE_NB_SAMPLES; i++){
		flatline_ch1[i] = FLATLINE_AMPL;
		flatline_ch2[i] = DAC_MID_VALUE;
	}

	new_state(IDLE);
	HAL_DAC_Init(&hdac);
	HAL_TIM_Base_Start_IT(&htim6);

	new_state(SINE_WAVE);
	//new_state(PULSE_TRAIN);


	while (1)
	{
		if (transition) {
			transition = false;

			// Stop DMA if it is currently in function
			if (dma_started) {
				HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
				HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_2);
				dma_started = false;
			}

			switch(curr_state){
			case IDLE:
				led_off(ORANGE_LED);
				break;
			case SINE_WAVE:
				led_on(ORANGE_LED);

				// Send sinewave data points to channels 1 and 2.
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)sinewave_ch1, SINE_WAVE_NB_SAMPLES, DAC_ALIGN_12B_R);
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t*)sinewave_ch2, SINE_WAVE_NB_SAMPLES, DAC_ALIGN_12B_R);
				dma_started = true;
				break;
			case PULSE_TRAIN:
				led_on(ORANGE_LED);

				// Send pulse train data points to channels 1 and 2.
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)pulses_ch1, PULSES_NB_SAMPLES, DAC_ALIGN_12B_R);
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t*)pulses_ch2, PULSES_NB_SAMPLES, DAC_ALIGN_12B_R);
				dma_started = true;
				break;
			case FLATLINE:
				led_on(ORANGE_LED);

				// Send flatline data point to channels 1 and 2.
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)flatline_ch1, FLATLINE_NB_SAMPLES, DAC_ALIGN_12B_R);
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t*)flatline_ch2, FLATLINE_NB_SAMPLES, DAC_ALIGN_12B_R);
				dma_started = true;
				break;
			default:
				Error_Handler();
				break;
			}
		}


	}
}

void new_state(state newState){
	curr_state = newState;
	transition = true;
}
void SystemClock_Config(void)
{
	/* Tir� de en.STM32Cube_FW_F4_V1.24.0\Projects\STM32F4-Discovery\
		Examples\DAC\DAC_SignalsGeneration */
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		Error_Handler();
	}

	// HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_SYSCLK, RCC_MCODIV_1);
}

void TIM6_Init(void)
{

	TIM_MasterConfigTypeDef sMasterConfig;

	__HAL_RCC_TIM6_CLK_ENABLE();

	/*##-1- Configure the TIM peripheral #######################################*/
	/* Time base configuration */
	htim6.Instance = TIM6;
	htim6.Init.Period = TIM6_PERIOD;
	htim6.Init.Prescaler = 0;
	htim6.Init.ClockDivision = 0;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	HAL_TIM_Base_Init(&htim6);

	/* TIM6 TRGO selection */
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

	HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig);

	// Enable interrupt
	HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

// Initialize DAC
void DAC_Init(void){
	GPIO_InitTypeDef          GPIO_InitStruct;
	static DMA_HandleTypeDef  hdma_dac1, hdma_dac2;
	static DAC_ChannelConfTypeDef sConfig;

	/* Enable peripherals and GPIO Clocks */
	/* DAC Periph clock enable */
	__HAL_RCC_DAC_CLK_ENABLE();
	/* Enable GPIO clock ****************************************/
	__HAL_RCC_GPIOA_CLK_ENABLE();
	/* DMA1 clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DAC Init */
	hdac.Instance = DAC;
	HAL_DAC_Init(&hdac);

	/* DAC Channel config */
	sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1);
	HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2);

	/* Configure peripheral GPIO */
	/* DAC Channel1 GPIO pin configuration */
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* DAC Channel2 GPIO pin configuration */
	GPIO_InitStruct.Pin = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* Configure the DMA streams */
	/* Set the parameters to be configured for Channel1*/
	hdma_dac1.Instance = DMA1_Stream5;
	hdma_dac1.Init.Channel  = DMA_CHANNEL_7;
	hdma_dac1.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_dac1.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_dac1.Init.MemInc = DMA_MINC_ENABLE;
	hdma_dac1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_dac1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_dac1.Init.Mode = DMA_CIRCULAR;
	hdma_dac1.Init.Priority = DMA_PRIORITY_LOW;

	/* Set the parameters to be configured for Channel2*/
	hdma_dac2.Instance = DMA1_Stream6;
	hdma_dac2.Init.Channel  = DMA_CHANNEL_7;
	hdma_dac2.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_dac2.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_dac2.Init.MemInc = DMA_MINC_ENABLE;
	hdma_dac2.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_dac2.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_dac2.Init.Mode = DMA_CIRCULAR;
	hdma_dac2.Init.Priority = DMA_PRIORITY_LOW;

	HAL_DMA_Init(&hdma_dac1);
	HAL_DMA_Init(&hdma_dac2);

	/* Associate the initialized DMA handle to the the DAC handle */
	__HAL_LINKDMA(&hdac, DMA_Handle1, hdma_dac1);
__HAL_LINKDMA(&hdac, DMA_Handle2, hdma_dac2);

	/* Configure the NVIC for DMA */
	/* Enable the DMA1 Stream5 IRQ Channel */
	HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

	/* Enable the DMA1 Stream6 IRQ Channel */
	HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
}



// Set-up LED outputs
void led_config(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOC_CLK_ENABLE();

	// Red LED, pin PC13
	GPIO_InitStruct.Pin = GPIO_PIN_13;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	// Yellow LED, pin PC12
	GPIO_InitStruct.Pin = GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

// Power on LED
void led_on(LEDcolor_typeDef ledColor)
{
	switch(ledColor)
	{
	case RED_LED:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
		break;
	case ORANGE_LED:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
		break;
	default:
		break;
	}
}

// Power off LED
void led_off(LEDcolor_typeDef ledColor)
{
	switch(ledColor)
	{
	case RED_LED:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
		break;
	case ORANGE_LED:
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
		break;
	default:
		break;
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	switch(curr_state) {
	case IDLE:
		// Do nothing.
		break;
	case SINE_WAVE:
		sine_wave_samp_count--;
		if (sine_wave_samp_count == 0) {
			new_state(FLATLINE);
		}
		break;
	case PULSE_TRAIN:
		pulses_samp_count--;
		if (pulses_samp_count == 0) {
			new_state(IDLE);
		}
		break;
	case FLATLINE:
		flatline_samp_count--;
		if (flatline_samp_count == 0) {
			new_state(PULSE_TRAIN);
		}
		break;
	default:
		Error_Handler();
		break;
	}
}

void button_pressed_callback(){
	// Begin new sequence only if no sequence is currently running.

	if (curr_state == IDLE){
		new_state(SINE_WAVE);
	}
}


void Error_Handler(void)
{
	// Power red LED
	led_on(RED_LED);
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif /* USE_FULL_ASSERT */
